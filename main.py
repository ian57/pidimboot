###
# Script by Ezechiel
# Date : 2019/05/26
# Description :  PiDimBoot
###

import pygame, os
from pygame.locals import *
from time import *

from const import *
from button import *
from interaction import *
from settings import *
from database import *
from system import *

#Raspberry Pi Small TFT Touchscreen Config
#Uncomment to activate
# Setup TFTScreen
# Comment when using hdmi anbd fbcp

if osType == "RPI":
	os.environ['SDL_VIDEODRIVER']= 'fbcon'
	#os.environ['SDL_VIDEODRIVER']= 'svgalib'
	#os.environ['SDL_VIDEODRIVER']= 'directfb'
	os.environ['SDL_FBDEV']= '/dev/fb1'
	# Touchscreen configuration
	os.environ['SDL_MOUSEDRV'] = 'TSLIB'
	os.environ["SDL_MOUSEDEV"] = '/dev/input/touchscreen'
	#os.environ["SDL_MOUSEDEV"] = '/dev/input/event0'

#load database
database = Database()

#start pygame
pygame.init()

if osType == "RPI":
	#Hide mouse
	pygame.mouse.set_visible(False)

# when cloning hdmi -> tft screen with fbcp, 640x480 resolution is minimum on hdmi pour pygame
# need to resisze all the graphic stuff to 640x480!!!
#screen = pygame.display.set_mode((640,480),pygame.FULLSCREEN)
screen = pygame.display.set_mode((480, 320))

img_bg = pygame.image.load(MAIN_IMG).convert() #background img loaded
img_black = pygame.image.load(BLACK_IMG).convert() #black img for sleep mode

navigation = 'HOME' #start nav menu

#Autoboot Rom
autoboot = False
file_autoboot = open(AUTOBOOT_DIR, "r")
value_autoboot = file_autoboot.readline()
value_autoboot = value_autoboot.replace(' ', '')
value_autoboot = value_autoboot.replace('\n', '')
file_autoboot.close

if len(value_autoboot) > 3:
	if os.path.isfile(ROM_DIR + value_autoboot):
		autoboot = True

if timeout > -1:
	timeoutClock = pygame.time.Clock()
else:
	timeoutClock = 0

currentTime = 0
# configure gpio for sleep mode
if backlightPWM:
        cmd = "gpio -g pwm 18 1024 && gpio -g mode 18 pwm"
        subprocess.call(cmd, shell=True)

while True:
	if autoboot:
		img_tmp = pygame.image.load(BACKGROUND_WHITE).convert()
		screen.blit(img_tmp, (0,0))
		
		fontLabel(pygame, screen, ROBOTOBLACK_TTF, "Autoboot", 50, 35, 120, RED)
		fontLabel(pygame, screen, ROBOTOBLACK_TTF, "Wait don't touch screen", 20, 15, 200, RED)
		pygame.display.flip() #refresh screen
		
		system = System(pygame)
		
		sleep(40) #Wait Naomi Startup
		
		screen.blit(img_bg, (0,0))
		pygame.display.flip() #refresh screen
		
		system.setCurrentGame(value_autoboot)	
		system.setAutoboot(True)
		system.sendingGame
		system.setAutoboot(False)
		
		sleep(2)
	else :
		navigation, currentTime  = Interaction().interaction(navigation, pygame, timeoutClock, currentTime)
		#print navigation
		#homepage interface
		if navigation == 'HOME':
			screen.blit(img_bg,(0,0)) #apply background

			createButton(pygame, screen, BUTTON_BIG_BLUE, ROBOTOBLACK_TTF, "SYSTEM", 36, 170, 120 , WHITE, 80)
			createButton(pygame, screen, BUTTON_BIG_BLUE, ROBOTOBLACK_TTF, "FAVORITE", 36, 155, 190 , WHITE, 65)
			createButton(pygame, screen, BUTTON_BIG_BLUE, ROBOTOBLACK_TTF, "SETTINGS", 36, 155, 260 , WHITE, 65)
			createButton(pygame, screen, BUTTON_POWEROFF, ROBOTOBLACK_TTF, "", 30, 410, 255 , WHITE, 0, 0)
			
		#System interface
		if navigation == 'SYSTEM':
			system = System(pygame)
			system.createInterface(screen)
			
		#Favorite interface
		if navigation == 'FAVORITE':
			system = System(pygame)
			system.createFavoriteInterface(screen)
			
		#setting interface
		if navigation == 'SETTINGS':
			setting = Setting()
			setting.createInterface(pygame, screen)
			
			
		pygame.display.flip() #refresh screen
	
	if not autoboot:
		#print 'not autoboot'
		#pygame.event.wait()
		# count time without action on touchscreen
		# if timeout, go to sleep mode
		if timeout > -1:
			currentTime += timeoutClock.tick()
			#print(str(currentTime))
			if (currentTime > timeout):
				#print("Timeout")
				if backlightPWM:
					for blight in range(1024, -1, -16):
						cmd = "gpio -g pwm 18 " + str(blight)
						subprocess.call(cmd, shell=True)
				else:
					#print('blackBG')
					screen.blit(img_black, (0, 0))
					pygame.display.flip()  # refresh screen

				pygame.event.wait()
				pygame.event.get()
				currentTime = timeoutClock.tick()

				if backlightPWM:
					for blight in range(0, 1025, 16):
						cmd = "gpio -g pwm 18 " + str(blight)
						subprocess.call(cmd, shell=True)

				currentTime = timeoutClock.tick()
	else:
		autoboot = False
		
	sleep(0.1)
