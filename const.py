###
# Script by Ezechiel
# Date : 2019/05/26
# Description :  PiDimBoot constantes
###
osType = "PC" # "PC" or "RPI"

timeout = 120000 #in ms. -1 -> no sleep mode
backlightPWM = False # True for screen with backlight management on gpio 18

if osType == "PC":
    mainBOOTDIR = '/home/yann/Gitlab/Gitlab/pidimboot/boot/'
    mainOPTDIR = '/home/yann/Gitlab/Gitlab/pidimboot/'
    romsDIR = '/home/yann/Gitlab/Gitlab/pidimboot/roms/'

if osType == "RPI":
    mainBOOTDIR = '/boot/pidimboot/'
    mainOPTDIR = '/opt/pidimboot/'
    romsDIR = '/home/pi/pidimboot/roms/'

#Dir
#ROM_DIR = mainBOOTDIR + 'roms/'
ROM_DIR = romsDIR
VIDEO_DIR = mainBOOTDIR + 'video/'
IMAGE_DIR = mainBOOTDIR + 'img/'
IMAGE_GAME_DIR = mainOPTDIR + 'img/games/'
AUTOBOOT_DIR = mainBOOTDIR + 'autoboot.txt'

#Policies

ARCADE_TTF = mainOPTDIR + 'fonts/Arcade.ttf'
#STARCRAFT_TTF = mainOPTDIR + 'fonts/Starcraft.ttf'
STARCRAFT_TTF = mainOPTDIR + 'fonts/Roboto-Black.ttf'
MEGADRIVE_TTF = mainOPTDIR + 'fonts/Megadrive.ttf'
#SEGAZ_TTF = mainOPTDIR + 'fonts/Segaz.ttf'
SEGAZ_TTF = mainOPTDIR + 'fonts/Roboto-Medium.ttf'
OPENSANSBOLD_TTF = mainOPTDIR + 'fonts/OpenSans-Bold.ttf'
OPENSANSEXTRABOLD_TTF = mainOPTDIR + 'fonts/OpenSans-ExtraBold.ttf'
DEJAVUSANSBOLD_TTF = mainOPTDIR + 'fonts/DejaVuSans-Bold.ttf'
ROBOTOBLACK_TTF = mainOPTDIR + 'fonts/Roboto-Black.ttf'
ROBOTOMEDIUM_TTF = mainOPTDIR + 'fonts/Roboto-Medium.ttf'

#Database
DATABASE = mainOPTDIR + 'database/pidimboot.db'

#Color
BLACK = 0, 0, 0
WHITE = 255, 255, 255
GREY = 197, 200, 204
DARKGREY = 70, 70, 70
RED = 240, 0, 0
GREEN = 19, 244, 7
DARKGREEN = 0, 135, 0
DARKRED = 135, 0, 0
BLUE = 0, 0, 240
DARKBLUE = 0, 0, 135
ORANGE = 243, 100, 24

#Backgroung pictures
MAIN_IMG = mainOPTDIR +  'img/src/main.jpg'
BLACK_IMG = mainOPTDIR +  'img/src/blackScreen.png'
SETTING_IMG = mainOPTDIR +  'img/src/setting.jpg'
LOGO_NAOMI = mainOPTDIR +  'img/src/naomi_small_logo.png'
LOGO_CHIHIRO = mainOPTDIR +  'img/src/chihiro_small_logo.png'
LOGO_TRIFORCE = mainOPTDIR +  'img/src/Triforce_small_logo.png'
BACKGROUND_WHITE = mainOPTDIR +  'img/src/background_white.jpg'
BACKGROUND_NOGAME = mainOPTDIR +  'img/src/background_no_game.jpg'
BACKGROUND_ATOMISWAVE = mainOPTDIR +  'img/src/background_atomiswave.jpg'
BACKGROUND_NAOMI = mainOPTDIR +  'img/src/background_naomi.jpg'
BACKGROUND_NAOMI2 = mainOPTDIR +  'img/src/background_naomi2.jpg'
BACKGROUND_TRIFORCE = mainOPTDIR +  'img/src/background_triforce.jpg'
BACKGROUND_CHIHIRO = mainOPTDIR +  'img/src/background_chihiro.jpg'
BACKGROUND_ALL_GAMES = mainOPTDIR +  'img/src/background_allgames.jpg'



#Button
BUTTON_BIG_BLUE = mainOPTDIR +  'img/src/button.png'
BUTTON_BACK = mainOPTDIR +  'img/src/back.png'
BUTTON_BACK_BLACK = mainOPTDIR +  'img/src/back_black.png'
BUTTON_BACK_NO_TRANSPARENT = mainOPTDIR +  'img/src/back_noTransparent.jpg'
BUTTON_POWEROFF = mainOPTDIR +  'img/src/poweroff.png'
#BUTTON_CHOICE_GREEN_SMALL = mainOPTDIR +  'img/src/choice_green.png'
#BUTTON_CHOICE_ROSE_SMALL = mainOPTDIR +  'img/src/choice_rose.png'
BUTTON_CHOICE_GREEN_SMALL = mainOPTDIR +  'img/src/choice_darkgreen.png'
BUTTON_CHOICE_ROSE_SMALL = mainOPTDIR +  'img/src/choice_red.png'
BUTTON_DECR = mainOPTDIR +  'img/src/button_decr.png'
BUTTON_INCR = mainOPTDIR +  'img/src/button_incr.png'
BUTTON_PREV = mainOPTDIR +  'img/src/prev.png'
BUTTON_NEXT = mainOPTDIR +  'img/src/next.png'
BUTTON_FAVORITE_OFF = mainOPTDIR +  'img/src/button_favorite_off.png'
BUTTON_FAVORITE_ON = mainOPTDIR +  'img/src/button_favorite_on.png'
#BUTTON_METAL = mainOPTDIR +  'img/src/button_metal.png'
#BUTTON_METAL_GREEN = mainOPTDIR +  'img/src/button_metal_green.png'
BUTTON_METAL = mainOPTDIR +  'img/src/button_metal_violet.png'
BUTTON_METAL_GREEN = mainOPTDIR +  'img/src/button_metal_violet.png'

#System
ATOMISWAVE = "ATOMISWAVE"
NAOMI1 = "NAOMI 1"
NAOMI2 = "NAOMI 2"
CHIHIRO = "CHIHIRO"
TRIFORCE = "TRIFORCE"

GAMES = "GAMES"
RACING = "RACING"
SHOOTER = "SHOOTER"
ACTION = "ACTION"
SPORT = "SPORT"
FIGHTING = "FIGHTING"
HORI_SHOOTEMUP = "H-SHOOTEMUP"
VERT_SHOOTEMUP = "V-SHOOTEMUP"
VARIOUS = "VARIOUS"
PUZZLE = "PUZZLE"
