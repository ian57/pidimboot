Naomi NetBoot based on PiDimBoot
============

![Naomi NetBoot Splashscreen](https://gitlab.com/ian57/pidimboot/-/raw/master/splashscreen/splashscreen2.png)

## For the impatient :) 

[![pidimboot sd card image](https://gitlab.com/ian57/pidimboot/-/raw/master/img/src/sdNaomiNetboot.png)](https://mega.nz/file/o5UxXAzI#9PnPvnSd2mBdtCQsdxuxhTX19hvLYNJiyKRTCyd6jQM)  
 * <https://mega.nz/file/o5UxXAzI#9PnPvnSd2mBdtCQsdxuxhTX19hvLYNJiyKRTCyd6jQM> 



## Introduction

Naomi NetBoot is based on the great PiDimBoot project (<https://gitlab.com/ezechiel/pidimboot>) from ezechiel, also based on PiforceTools (<https://github.com/travistyoj/piforcetools>) to load games to Sega Netdimm (Naomi 1 and 2, TriForce, Chihiro) with Raspberry and Lcd resistive touch screen 3.5" (320x480).

I used a geekcreit 3.5" 480x320 cheap screen from <https://fr.banggood.com/3_5-Inch-320-X-480-TFT-LCD-Display-Touch-Board-For-Raspberry-Pi-2-Model-B-RPI-B-p-1023432.html> which is a clone from the waveshare https://www.waveshare.com/wiki/3.5inch_RPi_LCD_(A) (backlight cannot be tuned, you'll need the revB or revC to do that <https://www.waveshare.com/wiki/3.5inch_RPi_LCD_(B)>, <https://www.waveshare.com/wiki/3.5inch_RPi_LCD_(C)>)

My code source is 100% FREE, OPEN SOURCE, and FORKABLE :)
Thanks no business, no money for this application, it's only FREE

## What's new?

This fork uses the **last Rasbian OS Lite** (Raspbian GNU/Linux 10) to avoid unuseful software (like xorg), and make a lighter image (1.6 GB without roms). 

```
Sys. de fichiers blocs de 1K Utilisé Disponible Uti% Monté sur
/dev/root            7261112 1626564    5306428  24% /
/dev/mmcblk0p1        258095   48804     209292  19% /boot
```

I change the **theme** to get closer to the Naomi original colors. Changed the fonts to be more readable (OMHO).

This project can be launched on a PC (for dev, or use) thanks to the "osType" switch in `const.py`. Just adapt the directories to your needs.

The "DIMM_CheckOff" patch is added, avoiding the cabinet to check ram before running the game, which saves precious time.


I added a "sleep mode " which switch off the screen after the fixed timeout, and a simple touch on the touchscreen switch it on again. In the case of a tft screen which allows backlight management (waveshare 3.5 revB or revC) the switch off is progressive by backlighting down the screen. In the case of a screen without backlight management, a black image is displayed to simulate switch off.  

see section "Sleep mode activation"

## New interface

![screenshot1](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen1.png)
![screenshot2](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen2.png)
![screenshot3](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen3.png)
![screenshot4](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen4.png)
![screenshot5](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen5.png)
![screenshot6](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen6.png)
![screenshot7](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen7.png)
![screenshot8](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen8.png)
![screenshot9](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen9.png)
![screenshot10](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen10.png)
![screenshot11](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen11.png)
![screenshot12](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen12.png)
![screenshot13](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen13.png)
![screenshot14](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen14.png)
![screenshot15](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen15.png)
![screenshot16](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen16.png)
![screenshot17](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen17.png)
![screenshot18](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screen18.png)

## Usage

1. Youtube Video presenting the new interface: 

[![pidimboot new interface](https://img.youtube.com/vi/FzK1-MPqmy4/0.jpg)](https://www.youtube.com/watch?v=FzK1-MPqmy4)   

2. List Games Compatibles : <https://drive.google.com/file/d/1AhWIBGnlyyTDlshie6sODfSJadcdYxgx/view>

Old interface, but same features :

2. Startup PiDimBoot : <https://youtu.be/7DgBk39FfiE>
3. Set Virtual Zero Key Chip : <https://youtu.be/M7UVBQ0CILQ>
4. Set IP Address by System : <https://youtu.be/dNdGQEcgTYQ>
5. Navigate to PiDimBoot. By System, By Genre : <https://youtu.be/xq9jMqT8gOU>
6. Launch Game : <https://youtu.be/vflhOEDOe5Q>
7. Favorite Add/Delete/Display Game : <https://youtu.be/9S-s0mjNalg>
8. Launch Autoboot One Game : <https://youtu.be/cApJJM8fajM>
9. Power OFF Raspberry and PiDimBoot : <https://youtu.be/XCpo7qEe0-U>

## Getting Started

You will need the following items to use Naomi Netboot:

1. A Raspberry Pi 3 - <http://www.raspberrypi.org/> (please don't use Pi4, it is actually impossible to rotate the screen without Xorg!)
2. An SD Card (Minimum 4GB, but I recommend at 8GB or 64GB for fullset games)
3. LCD Resistives Touch Screen 320x480 - (examples: <https://fr.banggood.com/3_5-Inch-320-X-480-TFT-LCD-Display-Touch-Board-For-Raspberry-Pi-2-Model-B-RPI-B-p-1023432.html> <https://www.amazon.fr/gp/product/B07NTH1JWH/ref=ppx_yo_dt_b_asin_image_o00_s00?ie=UTF8&psc=1>, another one which is cheaper is here https://fr.aliexpress.com/item/4001248404892.html. I'll give the configuration in the following.
4. A Naomi, Triforce, or Chihiro arcade system.
5. A Netdimm with a zero-key security PIC installed.  I cannot provide links for this, but a modicum of Google-fu will get you what you need.  The netdimm will need to be configured.
Example:

![netdimm](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/netdimm.jpg)
![eeprom 4.02 + zero key security PIC](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/naomieeprompic.jpg)
![zero key in place](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/naomizerokey.jpg)
![4.02 eeprom in place](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/naomieeprom4.02.jpg)
![Naomi check](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/naomiCheck.jpg)

6. A crossover network cable (see (https://en.wikipedia.org/wiki/Ethernet_crossover_cable))

## Installation
#### Easy install with img file

Here is a link to a fresh already installed Naomi Netboot image based on RaspbianOS Lite (Debian10): 

[![pidimboot sd card image](https://gitlab.com/ian57/pidimboot/-/raw/master/img/src/sdNaomiNetboot.png)](https://mega.nz/file/o5UxXAzI#9PnPvnSd2mBdtCQsdxuxhTX19hvLYNJiyKRTCyd6jQM)  

 * <https://mega.nz/file/o5UxXAzI#9PnPvnSd2mBdtCQsdxuxhTX19hvLYNJiyKRTCyd6jQM> 

Unzip and just burn the image on a 8GB µSD card, plug it and start the Raspberry Pi 3.

```
unzip NaomiNetboot_Pi3_8Go.zip 
sudo dd if=NaomiNetboot_Pi3_8Go.img of=/dev/mmcblk0 bs=1M status=progress && sync
```

on my NUC, `/dev/mmcblk0` is the SD card reader. You can use win32diskimager or Raspberry Pi Imager <https://www.raspberrypi.org/software/> to create the SD card. 

**Warning** According to your screen, you will perhaps need to recalibrate the touchscreen. Please refer the calibration paragraph below to see the procedure. 

#### Manually

###### Install OS and useful package on SD
* Download and install last Raspbian OS Lite <https://www.raspberrypi.org/downloads/raspbian/>, in fact Debian 10 Lite : <https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-05-28/2021-05-07-raspios-buster-armhf-lite.zip>
* With linux you can use `dd` to burn you SD :
```
unzip 2021-05-07-raspios-buster-armhf-lite.zip 
sudo dd if=2021-05-07-raspios-buster-armhf-lite.img of=/dev/mmcblk0 bs=1M status=progress && sync
``` 

**Actually, you should connect a screen and a keyboard to your pi or use SSH from another computer to make all the configurations before setting up the tft screen.**

![Both screens](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/bothscreens.jpg)

* Installing useful packages
In order to install Naomi Netboot and use the TFT screen we'll need some new packages
```
sudo apt-get remove --purge bluez bluez-firmware pi-bluetooth triggerhappy build-essential
sudo apt autoremove
sudo apt install --no-install-recommends evtest raspi-config libts-bin python-pygame fbi wiringpi libjpeg8 omxplayer 

```


* Install Git to get Naomi Netboot
```
sudo apt-get install git
```

###### Configure Autologin
To run Naomi Netboot at boot we need to autologin, for that just run `raspi-config`:
  ```
  sudo raspi-config
  ```
Choose `System Option` -> `Boot / Auto login` -> `Console Autologin`, validate and quit. You can reboot to see if it works.


###### Installing Naomi Netboot to `/opt`

Becoming root  : 
```
sudo -s
```
Install the Naomi NetBoot system in `/opt`
```
cd /opt
git clone https://gitlab.com/ian57/pidimboot.git pidimboot
```
Doing this, it will be easy to get the next update with a simple `git pull`

###### Installing TFT Drivers 
(most of 480x320 screen uses waveshare35a overlay driver)

Waveshare drivers are available from https://github.com/swkim01/waveshare-dtoverlays
```
git clone https://github.com/swkim01/waveshare-dtoverlays
cd waveshare-dtoverlays
sudo cp *.dtbo /boot/overlays/
```

In the case of the Waveshare 3.5 rev C there is a problem with the swkim01 overlays (touchscreen cannot be initialized see https://github.com/swkim01/waveshare-dtoverlays/issues/34). You'll need to used the original overlay from <https://github.com/waveshare/LCD-show> or from the waveshare-overlays directory of the project. This directory contains renamed versions of the waveshare github dtb files).

```
cd /opt/pidimboot/waveshare-dtoverlays
sudo cp waveshare35c.dtbo /boot/overlays/
```

###### Installing useful binaries and scripts 

I used `fbcp` with `omxplayer` and `fbv` to respectively display boot video and image. They are not included as package in raspbian OS so we need to copy them in `/usr/bin` directory :

```
cd /opt/pidimboot/binaries
sudo cp fbcp fbv2 /usr/bin
```

I made a small script to link all the command to display boot video and image

```
cd /opt/pidimboot/binaries
cp splash.sh /home/pi
chmod 755 /home/pi/splash.sh
```

```
#!/bin/bash
(/usr/bin/fbcp &) && /usr/bin/omxplayer --aspect-mode stretch /opt/pidimboot/splashscreen/introNaomiNetbootV2.mp4 &> /dev/null && /usr/bin/killall fbcp &> /dev/null
#clear
#sleep(1)
#FRAMEBUFFER=/dev/fb1 /usr/bin/fbv2 -e /opt/pidimboot/splashscreen/splashscreen3.png &> /dev/null
```

###### Configuring TFT screen

Add the following lines to `/boot/config.txt`, we need to rotate the screen with 90° , because the hardware resolution is 320x480, and not 480x320!
```
# comment the following hdmi* lines to get both screen (hdmi + tft on spi) 
#hdmi_force_hotplug=1
#hdmi_cvt=480 320 60 6 0 0 0
#hdmi_group=2
#hdmi_mode=87
dtparam=spi=on
dtoverlay=waveshare35a:rotate=90,speed=27000000
```

Now you can reboot, your screen should switch from white to black at boot. Nothing more. If it goes to black, the screen is configured, if not, something wrong happened! You need to check your are using the right overlay.

If you are using a Rev B or a Rev C of the waveshare screen, consider modifying the `dtoverlay` variable with the right overlays

```
# Rev A
#dtoverlay=waveshare35a:rotate=90,speed=27000000
# Rev B
#dtoverlay=waveshare35b:rotate=90,speed=27000000
# Rev C speed=125000000 -> 125MHz HighSpeed SPI
#dtoverlay=waveshare35c:rotate=270,speed=2400000000
```

This image uses the `waveshare35a.dtbo` overlays, which is a "standard" for chinese 3.5" 320x480 tft screens. If you use an adafruit screen, please consider to change to `pitft35-resistive.dtbo` in the `/boot/config.txt` file like that :

```
dtparam=spi=on
dtoverlay=pitft35-resistive,rotate=90,speed=20000000,fps=20
```
 
You can check that the right overlay is loaded a boot time with the following command :

```
sudo vcdbg log msg
```
a line like the following should be here :
```
001462.509: brfs: File read: /mfs/sd/overlays/waveshare35a.dtbo
001473.419: Loaded overlay 'waveshare35a'
001473.436: dtparam: rotate=90
001474.280: dtparam: speed=27000000
```
after you can check the screen is correctly configured with : 

```
$ dmesg | grep fb1
[    8.306043] graphics fb1: fb_ili9486 frame buffer, 480x320, 300 KiB video memory, 32 KiB buffer memory, fps=33, spi0.0 at 27 MHz
```
It should be ok/
###### Create udev rules (Example : For my screen, Adafruit is different value)
This allow the system to recognize the touchscreen as input. 

```
sudo nano /etc/udev/rules.d/29-ads7846.rules
```
add
```
SUBSYSTEM=="input", KERNEL=="event[0-9]*", ATTRS{name}=="ADS7846 Touchscreen", SYMLINK+="input/touchscreen"
```
reload udev rules : 
```
sudo udevadm control --reload-rules
```

Check `/dev/input/touchscreen` exists and you can use `evtest` to check that the touchscreen is working

```
# evtest 
No device specified, trying to scan all of /dev/input/event*
Available devices:
/dev/input/event0:	ADS7846 Touchscreen
Select the device event number [0-0]: 0
Input driver version is 1.0.1
Input device ID: bus 0x0 vendor 0x0 product 0x0 version 0x0
Input device name: "ADS7846 Touchscreen"
Supported events:
  Event type 0 (EV_SYN)
  Event type 1 (EV_KEY)
    Event code 330 (BTN_TOUCH)
  Event type 3 (EV_ABS)
    Event code 0 (ABS_X)
      Value   2357
      Min        0
      Max     4095
    Event code 1 (ABS_Y)
      Value   3593
      Min        0
      Max     4095
    Event code 24 (ABS_PRESSURE)
      Value      0
      Min        0
      Max      255
Properties:
```

###### Switching from PC arch to RPI arch

Just edit the `const.py` file like this
```
osType = "RPI" # "PC"or "RPI"
```
That's it!

###### IMPORTANT : Replacing the libsdl1.2

There is a bug with touchscreen between libsdl 1.2 and pygame. The original debian libsdl prevent Naomi Netboot from working, because touchscreen is not working well! We need to install a patched release of the library. 
See <https://www.raspberrypi.org/forums/viewtopic.php?t=250001> for more details.

```
sudo -s
cd /opt/pidimboot/SDLLibDebian
sudo dpkg -i libsdl1.2debian_1.2.15+veloci1-1_armhf.deb
```
and check the good installation

```
dpkg -l | grep sdl1.2
ii  libsdl1.2debian:armhf          1.2.15+veloci1-1                    armhf        Simple DirectMedia Layer
```

###### Calibrating the touchscreen
The touchscreen needs to be calibrated to get the right coordinates of your finger: 

```
sudo -s
TSLIB_FBDEVICE=/dev/fb1 TSLIB_TSDEVICE=/dev/input/touchscreen ts_calibrate
xres = 480, yres = 320
Took 14 samples...
Top left : X =  856 Y = 3560
Took 11 samples...
Top right : X =  888 Y =  640
Took 9 samples...
Bot right : X = 3155 Y =  591
Took 6 samples...
Bot left : X = 3222 Y = 3506
Took 4 samples...
Center : X = 2009 Y = 2082
516.207764 -0.002900 -0.130231
-31.107666 0.094908 -0.000567
Calibration constants: 33830192 -190 -8534 -2038672 6219 -37 65536
```
the calibration values wil be stored in `/etc/pointercal`

![screenshot1](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screenCal.png)
![screenshot2](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screenCal2.png)
![screenshot3](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screenCal3.png)
![screenshot4](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screenCal4.png)
![screenshot5](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screenCal5.png)

You can test the good calibration with the `ts_test` program

For Waveshare 3.5 rev A, if you don't want to calibrate you can create the `/etc/pointercal` file with these values

```
-90 -8340 33390348 6009 -220 -1079364 65536 480 320 0
```

For Waveshare 3.5 rev C, if you don't want to calibrate you can create the `/etc/pointercal` file with these values

```
184 8565 -2704012 -5589 -130 22035352 65536 480 320 0
```


```
sudo -s
TSLIB_FBDEVICE=/dev/fb1 TSLIB_TSDEVICE=/dev/input/touchscreen ts_test
```

![screenshot1](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screenTest.png)
![screenshot2](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/screenTest2.png)


###### Logging in Ram

To avoid numerous Read/Write on the SD card, which is not godd for its timelife, it is better to put logs in ram.

```
sudo nano /etc/fstab

tmpfs /tmp tmpfs defaults,noatime,nosuid,size=10m 0 0
tmpfs /var/tmp tmpfs defaults,noatime,nosuid,size=10m 0 0
tmpfs /var/log tmpfs defaults,noatime,nosuid,mode=0755,size=10m 0 0
```

###### Upload roms to Naomi Netboot
The roms are stored in `/home/pi/pidimboot/roms` and we create the `autoboot.txt` which is used to run a single game at boot time.
```
mkdir -p /home/pi/pidimboot/roms
sudo mkdir /boot/pidimboot
sudo touch /boot/pidimboot/autoboot.txt
```

To install roms on your SD, you'll need a linux OS to be able to read/write the ext4 filesystem from a card reader. You can transfer roms through SSH from a linux computer in the same network using `scp`.  

From a windows PC in the same network, you can transfer with SSH through network with `winscp` thanks to SFTP protocol.

![winscp1](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/winscp.png)

Use the raspberry pi IP `192.168.1.99` login `pi` password `raspberry`. 

![winscp2](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/winscp2.png)

accept the connection

![winscp3](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/winscp3.png)

now you can transfer easily roms to the `/home/pi/pidimboot/roms` directory

###### Launching automatically Application Naomi Netboot at boot

```
cd ~
nano .bashrc
```
scroll to the end of the file and add
``` 
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
	echo "Welcome SSH"
else
  clear
  sudo python /opt/pidimboot/main.py
fi
```

###### Set Ip static
We need to put our raspberry pi and the Netdimm in the same network to get them connected. So we are going to fix the IP of the raspberry pi to `192.168.1.99` and the netdimm should get an IP address in this network class.  
We need to edit the `/etc/dhcpcd.conf` file
```
sudo nano /etc/dhcpcd.conf
```
and add at the end of the file

```
# fallback to static profile on eth0
interface eth0
#fallback static_eth0
static ip_address=192.168.1.99/24
static routers=192.168.1.254
# in case of a local network connected and configured to internet
# to be able to get packages and make updates (google DNS ip address)
static domain_name_servers=8.8.8.8
```

###### Adding a new game in the system
We need first to add a new entry in the file  `gamelist.py`  : 

```
 SHOOTER: {
                "Confidential Mission":                "ConfidentialMission.bin",
                "Death Crimson OX":                     "DeathCrimsonOX.bin",
                "House of the Dead 2":                "HouseOfTheDead2.bin", 
                "Jambo Safari":                         "Jambo_Safari.bin",
                "Lupin 3 The Shooting":                "Lupin3_TheShooting.bin",
                "Maze of the King":                     "TheMazeOfTheKings.bin"
            },
```            

Here we added `"House of the Dead 2":                "HouseOfTheDead2.bin",`

Be careful, your Rom filename needs to be named `HouseOfTheDead2.bin`

We need to add it also in the database which contains all the data of the game (description, system, type, number of players, etc.)

For that you have to edit the file `"database/pidimboot.db"` with a software that allows to read sqllite database. Under linux you can do this with `sqlitebrowser` for example :

![sqlitebrowser](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/sqlitebrowser.png)

You need to add images also : 

![hotd2withoutIMG](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/hotd2.png)

###### Adding boot video splashscreen

Here is the `splashscreen.service file`

```
[Unit]
Description=Splash screen
DefaultDependencies=no
After=local-fs.target

[Service]
#ExecStart=/usr/bin/fbi -d /dev/fb1 --noverbose -a /opt/pidimboot/splashscreen/splashscreen.png
ExecStart=/home/pi/splash.sh
StandardInput=tty
StandardOutput=tty

[Install]
WantedBy=sysinit.target
```
Copy it in the right place and activate the service
```
cd /opt/pidimboot/
sudo cp splashscreen.service /etc/systemd/system
sudo systemctl enable splashscreen.service
``` 
That's it!

###### Tuning `/boot/cmdline.txt`

To avoid ugly console log at boot, it should look like this by adding `logo.nologo quiet` at the end of the line

```
console=serial0,115200 console=tty1 loglevel=3 vt.global_cursor_default=0 root=PARTUUID=74f8f662-02 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait logo.nologo quiet
```
Reboot and you'll be able to upload game in your Netdimm...

###### Sleep mode activation

There are two ways to operate sleep mode : 
 * with backlight management (for screen which handle this feature)
 * by displaying a black image on screen (for screen without backlight management)

To use the first feature, you'll need a tft screen with backlight management. Most of the time it is done through the GPIO 18 and a PWM control. 

This feature was developped for Waveshare 3.5 revB and revC tft screens : 
 * https://www.waveshare.com/wiki/3.5inch_RPi_LCD_(B)
 * https://www.waveshare.com/wiki/3.5inch_RPi_LCD_(C)

According to the Waveshare wiki, to allow backlight management you need to solder a 0R resistor on this pad or solder directly to connect (see below). 

![screenshot1](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/waveshareRevCSoldering.jpg)

After that, you just need to tune the `timeout` in the `/opt/pidimboot/consts.py` file.
change this line from :

```
timeout = -1 #in ms. -1 -> no sleep mode
backlightPWM = False # True for screen with backlight management on gpio 18
```
to 
```
timeout = 90000 #in ms. -1 -> no sleep mode here we have 1 minutes 30s before sleep mode
backlightPWM = True # True for screen with backlight management on gpio 18
```
to activate screen switch off 1m30s timeout after the last action on the touchscreen.

[![sleep mode with backlight](https://img.youtube.com/vi/i3WND4TNgwk/0.jpg)](https://www.youtube.com/watch?v=i3WND4TNgwk)

To use this feature with the second type of screen (waveshare 3.5" revA for example)

```
timeout = 90000 #in ms. -1 -> no sleep mode here we have 1 minutes 30s before sleep mode
backlightPWM = False # True for screen with backlight management on gpio 18
```

You will get a black screen after 1m30s of inactivity.

[![sleep mode with backlight](https://img.youtube.com/vi/yP-XVZBVGYY/0.jpg)](https://www.youtube.com/watch?v=yP-XVZBVGYY)


###### Running the project

![screenshot1](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/netbootUpload.jpg)
![screenshot2](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/netbootUploadMS6.jpg)
![screenshot3](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/netbootUploadDOA.jpg)
![screenshot4](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/netbootUploadDOA2.jpg)
![screenshot5](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/netbootUploadDOA3.jpg)
![screenshot5](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/netboot2Gens.jpg)
![screenshot6](https://gitlab.com/ian57/pidimboot/-/raw/master/img/screenshots/netbootUploadBorderDown.jpg)
## Credits

* Thanks ezechiel for his great job on pidimboot <https://gitlab.com/ezechiel/pidimboot>
* Thanks inquisitom for his new UI and new snapshots  
* Thanks triforcetools and travistyoj for PiforceTools.
* Thanks darksoft for killer Atomiswave conversions.
* Thanks Mr_lune (NeoArcadia) for Covers Picture artset game.
* Thanks Aetios, Squallrs et Pourq (NeoArcadia) for design and create STL Naomi Cabinet Integration.
